/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POSTTEST3;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Acer
 */
public class LembarBerhasil extends javax.swing.JFrame {

    /**
     * Creates new form LembarBerhasil
     */
    public LembarBerhasil() {
        initComponents();
    }

    LembarBerhasil(JFrame jFrame, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    public JLabel getnamalengkap2(){
        return namalengkap2;
    }
    
    public JLabel getnim2(){
        return nim2;
    }
    
    public JLabel getjeniskelamin2(){
        return jeniskelamin2;
    }
    
    public JLabel getttl2(){
        return ttl2;
    }
    
    public JLabel gethari2(){
        return hari2;
    }
    
    public JLabel getbulan2(){
        return bulan2;
    }
    
    public JLabel gettahun2(){
        return tahun2;
    }
    
    public JLabel getdosenpembimbing2(){
        return dosenpembimbing2;
    }
    
    public JLabel getpembayaran2(){
        return pembayaran2;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        registrasiberhasil = new javax.swing.JLabel();
        namalengkap2 = new javax.swing.JLabel();
        nim2 = new javax.swing.JLabel();
        jeniskelamin2 = new javax.swing.JLabel();
        ttl2 = new javax.swing.JLabel();
        dosenpembimbing2 = new javax.swing.JLabel();
        pembayaran2 = new javax.swing.JLabel();
        namalengkap = new javax.swing.JLabel();
        nim = new javax.swing.JLabel();
        jeniskelamin = new javax.swing.JLabel();
        ttl = new javax.swing.JLabel();
        dosenpembimbing = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        hari2 = new javax.swing.JLabel();
        bulan2 = new javax.swing.JLabel();
        tahun2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 0, 0));

        registrasiberhasil.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        registrasiberhasil.setForeground(new java.awt.Color(255, 255, 255));
        registrasiberhasil.setText("REGISTRASI BERHASIL");

        namalengkap2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        namalengkap2.setForeground(new java.awt.Color(255, 255, 255));
        namalengkap2.setText("NAMA LENGKAP");

        nim2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nim2.setForeground(new java.awt.Color(255, 255, 255));
        nim2.setText("NIM");

        jeniskelamin2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jeniskelamin2.setForeground(new java.awt.Color(255, 255, 255));
        jeniskelamin2.setText("JENIS KELAMIN");

        ttl2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ttl2.setForeground(new java.awt.Color(255, 255, 255));
        ttl2.setText("TTL");

        dosenpembimbing2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        dosenpembimbing2.setForeground(new java.awt.Color(255, 255, 255));
        dosenpembimbing2.setText("DOSEN PEMBIMBING");

        pembayaran2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        pembayaran2.setForeground(new java.awt.Color(255, 255, 255));
        pembayaran2.setText("PEMBAYARAN");

        namalengkap.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        namalengkap.setForeground(new java.awt.Color(255, 255, 255));
        namalengkap.setText("NAMA LENGKAP                 :");

        nim.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nim.setForeground(new java.awt.Color(255, 255, 255));
        nim.setText("NIM                                   :");

        jeniskelamin.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jeniskelamin.setForeground(new java.awt.Color(255, 255, 255));
        jeniskelamin.setText("JENIS KELAMIN                  :");

        ttl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ttl.setForeground(new java.awt.Color(255, 255, 255));
        ttl.setText("TTL                                    :");

        dosenpembimbing.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        dosenpembimbing.setForeground(new java.awt.Color(255, 255, 255));
        dosenpembimbing.setText("DOSEN PEMBIMBING          :");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("PEMBAYARAN                    :");

        hari2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        hari2.setForeground(new java.awt.Color(255, 255, 255));
        hari2.setText("HARI");

        bulan2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bulan2.setForeground(new java.awt.Color(255, 255, 255));
        bulan2.setText("BULAN");

        tahun2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tahun2.setForeground(new java.awt.Color(255, 255, 255));
        tahun2.setText("TAHUN");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(119, 119, 119)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jeniskelamin)
                            .addComponent(namalengkap)
                            .addComponent(nim)
                            .addComponent(ttl)
                            .addComponent(dosenpembimbing)
                            .addComponent(jLabel6))
                        .addGap(23, 23, 23)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(namalengkap2)
                            .addComponent(nim2)
                            .addComponent(jeniskelamin2)
                            .addComponent(dosenpembimbing2)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(ttl2)
                                .addGap(18, 18, 18)
                                .addComponent(hari2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(bulan2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tahun2))
                            .addComponent(pembayaran2)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(194, 194, 194)
                        .addComponent(registrasiberhasil)))
                .addContainerGap(105, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(registrasiberhasil)
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(namalengkap2)
                    .addComponent(namalengkap))
                .addGap(39, 39, 39)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nim2)
                    .addComponent(nim))
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jeniskelamin2)
                    .addComponent(jeniskelamin))
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ttl2)
                    .addComponent(ttl)
                    .addComponent(hari2)
                    .addComponent(bulan2)
                    .addComponent(tahun2))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dosenpembimbing)
                    .addComponent(dosenpembimbing2))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jLabel6))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(pembayaran2)))
                .addContainerGap(76, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

              
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LembarBerhasil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LembarBerhasil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LembarBerhasil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LembarBerhasil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                LembarBerhasil dialog = new LembarBerhasil(new javax.swing.JFrame(),true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing (java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
        
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bulan2;
    private javax.swing.JLabel dosenpembimbing;
    private javax.swing.JLabel dosenpembimbing2;
    private javax.swing.JLabel hari2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jeniskelamin;
    private javax.swing.JLabel jeniskelamin2;
    private javax.swing.JLabel namalengkap;
    private javax.swing.JLabel namalengkap2;
    private javax.swing.JLabel nim;
    private javax.swing.JLabel nim2;
    private javax.swing.JLabel pembayaran2;
    private javax.swing.JLabel registrasiberhasil;
    private javax.swing.JLabel tahun2;
    private javax.swing.JLabel ttl;
    private javax.swing.JLabel ttl2;
    // End of variables declaration//GEN-END:variables

}
