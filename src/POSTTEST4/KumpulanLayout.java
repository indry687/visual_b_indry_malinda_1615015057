/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POSTTEST4;

/**
 *
 * @author Acer
 */
public class KumpulanLayout extends javax.swing.JFrame {

    /**
     * Creates new form KumpulanLayout
     */
    public KumpulanLayout() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelborder1 = new javax.swing.JPanel();
        katahome = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cardbiodata = new javax.swing.JPanel();
        nama = new javax.swing.JLabel();
        ttl = new javax.swing.JLabel();
        alamat = new javax.swing.JLabel();
        pekerjaan = new javax.swing.JLabel();
        namasaya = new javax.swing.JLabel();
        ttlsaya = new javax.swing.JLabel();
        alamatsaya = new javax.swing.JLabel();
        pekerjaansaya = new javax.swing.JLabel();
        fotocuy = new javax.swing.JLabel();
        cardkesanpesan = new javax.swing.JPanel();
        pesan = new javax.swing.JLabel();
        kesan = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        panelborder2 = new javax.swing.JPanel();
        panelgridbutton = new javax.swing.JPanel();
        buttonhome = new javax.swing.JButton();
        buttonbiodata = new javax.swing.JButton();
        buttonkesanpesan = new javax.swing.JButton();
        panelborder3 = new javax.swing.JPanel();
        panelborder4 = new javax.swing.JPanel();
        panelborder5 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelborder1.setBackground(new java.awt.Color(153, 153, 255));
        panelborder1.setLayout(new java.awt.CardLayout());

        katahome.setBackground(new java.awt.Color(204, 204, 255));

        jLabel1.setFont(new java.awt.Font("Gungsuh", 0, 36)); // NOI18N
        jLabel1.setText("SELAMAT DATANG DI WELCOME");

        javax.swing.GroupLayout katahomeLayout = new javax.swing.GroupLayout(katahome);
        katahome.setLayout(katahomeLayout);
        katahomeLayout.setHorizontalGroup(
            katahomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(katahomeLayout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(jLabel1)
                .addContainerGap(71, Short.MAX_VALUE))
        );
        katahomeLayout.setVerticalGroup(
            katahomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(katahomeLayout.createSequentialGroup()
                .addGap(93, 93, 93)
                .addComponent(jLabel1)
                .addContainerGap(222, Short.MAX_VALUE))
        );

        panelborder1.add(katahome, "card2");

        cardbiodata.setBackground(new java.awt.Color(204, 204, 255));

        nama.setFont(new java.awt.Font("Lucida Handwriting", 0, 14)); // NOI18N
        nama.setText("NAMA                 :");

        ttl.setFont(new java.awt.Font("Lucida Handwriting", 0, 14)); // NOI18N
        ttl.setText("TTL                     :");

        alamat.setFont(new java.awt.Font("Lucida Handwriting", 0, 14)); // NOI18N
        alamat.setText("ALAMAT              :");

        pekerjaan.setFont(new java.awt.Font("Lucida Handwriting", 0, 14)); // NOI18N
        pekerjaan.setText("PEKERJAAN         :");

        namasaya.setFont(new java.awt.Font("Lucida Handwriting", 0, 14)); // NOI18N
        namasaya.setText("INDRY MALINDA");

        ttlsaya.setFont(new java.awt.Font("Lucida Handwriting", 0, 14)); // NOI18N
        ttlsaya.setText("SAMARINDA, 22 JULI 1999");

        alamatsaya.setFont(new java.awt.Font("Lucida Handwriting", 0, 14)); // NOI18N
        alamatsaya.setText("JL. TATAKO SAMBUTAN");

        pekerjaansaya.setFont(new java.awt.Font("Lucida Handwriting", 0, 14)); // NOI18N
        pekerjaansaya.setText("MAHASISWA");

        fotocuy.setIcon(new javax.swing.ImageIcon("D:\\Tugas Indry\\TI B 2016\\SEMESTER 4\\VISUAL\\Netbeans\\VISUAL_B_INDRY_MALINDA_1615015057\\76543.jpg")); // NOI18N
        fotocuy.setText("jLabel2");

        javax.swing.GroupLayout cardbiodataLayout = new javax.swing.GroupLayout(cardbiodata);
        cardbiodata.setLayout(cardbiodataLayout);
        cardbiodataLayout.setHorizontalGroup(
            cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cardbiodataLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(fotocuy, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cardbiodataLayout.createSequentialGroup()
                        .addGroup(cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ttl)
                            .addComponent(alamat)
                            .addComponent(pekerjaan))
                        .addGap(60, 60, 60)
                        .addGroup(cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(alamatsaya)
                            .addComponent(ttlsaya)
                            .addComponent(pekerjaansaya)))
                    .addGroup(cardbiodataLayout.createSequentialGroup()
                        .addComponent(nama)
                        .addGap(60, 60, 60)
                        .addComponent(namasaya)))
                .addContainerGap(52, Short.MAX_VALUE))
        );
        cardbiodataLayout.setVerticalGroup(
            cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cardbiodataLayout.createSequentialGroup()
                .addGroup(cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cardbiodataLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(fotocuy))
                    .addGroup(cardbiodataLayout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addGroup(cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nama)
                            .addComponent(namasaya))
                        .addGap(26, 26, 26)
                        .addGroup(cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ttl)
                            .addComponent(ttlsaya))
                        .addGap(18, 18, 18)
                        .addGroup(cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(alamat)
                            .addComponent(alamatsaya))
                        .addGap(18, 18, 18)
                        .addGroup(cardbiodataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(pekerjaan)
                            .addComponent(pekerjaansaya))))
                .addContainerGap(72, Short.MAX_VALUE))
        );

        panelborder1.add(cardbiodata, "card3");

        cardkesanpesan.setBackground(new java.awt.Color(204, 204, 255));

        pesan.setFont(new java.awt.Font("Tunga", 1, 48)); // NOI18N
        pesan.setText("PESAN");

        kesan.setFont(new java.awt.Font("Tunga", 1, 48)); // NOI18N
        kesan.setText("KESAN");

        jLabel2.setIcon(new javax.swing.ImageIcon("D:\\Tugas Indry\\TI B 2016\\SEMESTER 4\\VISUAL\\Netbeans\\VISUAL_B_INDRY_MALINDA_1615015057\\as.jpg")); // NOI18N
        jLabel2.setText("jLabel2");

        jLabel3.setIcon(new javax.swing.ImageIcon("D:\\Tugas Indry\\TI B 2016\\SEMESTER 4\\VISUAL\\Netbeans\\VISUAL_B_INDRY_MALINDA_1615015057\\Bd57hC1IQAAfEFv.jpg")); // NOI18N
        jLabel3.setText("jLabel3");

        javax.swing.GroupLayout cardkesanpesanLayout = new javax.swing.GroupLayout(cardkesanpesan);
        cardkesanpesan.setLayout(cardkesanpesanLayout);
        cardkesanpesanLayout.setHorizontalGroup(
            cardkesanpesanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cardkesanpesanLayout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addComponent(kesan)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pesan)
                .addGap(121, 121, 121))
            .addGroup(cardkesanpesanLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 132, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(112, 112, 112))
        );
        cardkesanpesanLayout.setVerticalGroup(
            cardkesanpesanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cardkesanpesanLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(cardkesanpesanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pesan)
                    .addComponent(kesan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(cardkesanpesanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        panelborder1.add(cardkesanpesan, "card4");

        getContentPane().add(panelborder1, java.awt.BorderLayout.CENTER);

        panelborder2.setBackground(new java.awt.Color(255, 153, 153));

        panelgridbutton.setLayout(new java.awt.GridLayout(1, 0, 2, 0));

        buttonhome.setText("HOME");
        buttonhome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonhomeActionPerformed(evt);
            }
        });
        panelgridbutton.add(buttonhome);

        buttonbiodata.setText("BIODATA");
        buttonbiodata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonbiodataActionPerformed(evt);
            }
        });
        panelgridbutton.add(buttonbiodata);

        buttonkesanpesan.setText("KESAN DAN PESAN");
        buttonkesanpesan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonkesanpesanActionPerformed(evt);
            }
        });
        panelgridbutton.add(buttonkesanpesan);

        javax.swing.GroupLayout panelborder2Layout = new javax.swing.GroupLayout(panelborder2);
        panelborder2.setLayout(panelborder2Layout);
        panelborder2Layout.setHorizontalGroup(
            panelborder2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelgridbutton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 911, Short.MAX_VALUE)
        );
        panelborder2Layout.setVerticalGroup(
            panelborder2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelborder2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(panelgridbutton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(panelborder2, java.awt.BorderLayout.PAGE_START);

        panelborder3.setBackground(new java.awt.Color(255, 204, 153));

        javax.swing.GroupLayout panelborder3Layout = new javax.swing.GroupLayout(panelborder3);
        panelborder3.setLayout(panelborder3Layout);
        panelborder3Layout.setHorizontalGroup(
            panelborder3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 911, Short.MAX_VALUE)
        );
        panelborder3Layout.setVerticalGroup(
            panelborder3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        getContentPane().add(panelborder3, java.awt.BorderLayout.PAGE_END);

        panelborder4.setBackground(new java.awt.Color(153, 255, 153));

        javax.swing.GroupLayout panelborder4Layout = new javax.swing.GroupLayout(panelborder4);
        panelborder4.setLayout(panelborder4Layout);
        panelborder4Layout.setHorizontalGroup(
            panelborder4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        panelborder4Layout.setVerticalGroup(
            panelborder4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 357, Short.MAX_VALUE)
        );

        getContentPane().add(panelborder4, java.awt.BorderLayout.LINE_END);

        panelborder5.setBackground(new java.awt.Color(102, 255, 204));

        javax.swing.GroupLayout panelborder5Layout = new javax.swing.GroupLayout(panelborder5);
        panelborder5.setLayout(panelborder5Layout);
        panelborder5Layout.setHorizontalGroup(
            panelborder5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        panelborder5Layout.setVerticalGroup(
            panelborder5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 357, Short.MAX_VALUE)
        );

        getContentPane().add(panelborder5, java.awt.BorderLayout.LINE_START);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonhomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonhomeActionPerformed
        // TODO add your handling code here:
        katahome.setVisible(true);
        cardbiodata.setVisible(false);
        cardkesanpesan.setVisible(false);
    }//GEN-LAST:event_buttonhomeActionPerformed

    private void buttonbiodataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonbiodataActionPerformed
        // TODO add your handling code here:
        cardbiodata.setVisible(true);
        katahome.setVisible(false);
        cardkesanpesan.setVisible(false);
    }//GEN-LAST:event_buttonbiodataActionPerformed

    private void buttonkesanpesanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonkesanpesanActionPerformed
        // TODO add your handling code here:
        cardkesanpesan.setVisible(true);
        katahome.setVisible(false);
        cardbiodata.setVisible(false);
    }//GEN-LAST:event_buttonkesanpesanActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(KumpulanLayout.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(KumpulanLayout.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(KumpulanLayout.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(KumpulanLayout.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new KumpulanLayout().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel alamat;
    private javax.swing.JLabel alamatsaya;
    private javax.swing.JButton buttonbiodata;
    private javax.swing.JButton buttonhome;
    private javax.swing.JButton buttonkesanpesan;
    private javax.swing.JPanel cardbiodata;
    private javax.swing.JPanel cardkesanpesan;
    private javax.swing.JLabel fotocuy;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel katahome;
    private javax.swing.JLabel kesan;
    private javax.swing.JLabel nama;
    private javax.swing.JLabel namasaya;
    private javax.swing.JPanel panelborder1;
    private javax.swing.JPanel panelborder2;
    private javax.swing.JPanel panelborder3;
    private javax.swing.JPanel panelborder4;
    private javax.swing.JPanel panelborder5;
    private javax.swing.JPanel panelgridbutton;
    private javax.swing.JLabel pekerjaan;
    private javax.swing.JLabel pekerjaansaya;
    private javax.swing.JLabel pesan;
    private javax.swing.JLabel ttl;
    private javax.swing.JLabel ttlsaya;
    // End of variables declaration//GEN-END:variables
}
